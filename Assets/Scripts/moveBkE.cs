using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBkE : MonoBehaviour
{
    private Vector3 rightTopCameraBorder;
    private Vector3 siz;
    public float positionRestartX= -11.02f; // Flipped the X position for right-to-left scrolling
    public Vector2 speed;

    void Start()
    {
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0)); // Changed to top right corner
    }

    void Update()	
    {	
        GetComponent<Rigidbody2D>().velocity = new Vector2(speed.x, 0); // Changed to positive x velocity

        siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        if (transform.position.x > rightTopCameraBorder.x + (siz.x / 2)) // Changed to check for right edge of screen
        {
            transform.position = new Vector3(positionRestartX, transform.position.y, transform.position.z);
        }
    }
}
