using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class touchAction : MonoBehaviour
{
  public float jumpRotation = 10f;

    void Start()
    {
      
    }

    void Update()
    {
        Vector3 topScreenBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        if(transform.position.y > topScreenBorder.y)
        {
            transform.position = new Vector3(transform.position.x, topScreenBorder.y, transform.position.z);
        }
        if (Input.GetKeyDown("space") || Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {

          GetComponent<Rigidbody2D>().velocity = new Vector2(0,5);
          transform.Rotate(Vector3.forward * jumpRotation);

        }

    }
}
