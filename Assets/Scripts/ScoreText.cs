using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    private Text scoreText;

    void Start()
    {
        // Récupérer le composant Text du GameObject
        scoreText = GetComponent<Text>();
    }

    void Update()
    {
        // Mettre à jour le texte avec la valeur du score dans GameState
        scoreText.text = GameState.instance.GetScore().ToString();
    }
}