using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBk : MonoBehaviour
{
    private Vector3 leftBottomCameraBorder;
    private Vector3 siz;
    public float positionRestartX= 10.78f;
    public Vector2 speed;
    // Start is called before the first frame update
    void Start()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0,0));

    }

    // Update is called once per frame
void Update()	
{	
	GetComponent<Rigidbody2D>().velocity = new Vector2(-speed.x,0);												
    siz.x	=	gameObject.GetComponent<SpriteRenderer>().bounds.size.x;	
	siz.y	=	gameObject.GetComponent<SpriteRenderer>().bounds.size.y;	
									
//	If	the	backgound	exits	the	screen
//	Set	the	X	posi;on	with	the	original	backGround3	X	posi;on	
    if	(transform.position.x<	leftBottomCameraBorder.x-(siz.x	/2))	
    {	
        transform.position	=	new	Vector3(positionRestartX,transform.position.y,transform.position.z);	
    }	
}
}
