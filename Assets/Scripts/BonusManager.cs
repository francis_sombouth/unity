using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusManager : MonoBehaviour
{
    // Vecteur de mouvement du bonus
    public Vector2 movement;

    // Objet du bonus
    public GameObject bonus;

    // Transform d'origine du bonus
    private Transform bonusOriginalTransform;

    // Bordures de la caméra
    private Vector3 leftBottomCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;

    // Taille du sprite du bonus
    private Vector3 siz;

    // Appelé au début de l'exécution du script
    void Start()
    {
        // Récupération des bordures de la caméra en pixels
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        // Récupération du transform d'origine du bonus et de sa taille
        bonusOriginalTransform = bonus.transform;
        siz.x = bonus.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = bonus.GetComponent<SpriteRenderer>().bounds.size.y;

        // Appel répété de la fonction MoveBonus toutes les 5 secondes
        InvokeRepeating("MoveBonus", 5.0f, 5.0f);
    }

    // Déplace le bonus à une position aléatoire sur l'axe Y
    void MoveBonus()
    {
        float randomY = Random.Range(1, 4) - 2;
        float posX = rightBottomCameraBorder.x + (siz.x / 2);
        float posY = leftTopCameraBorder.y - 3f + randomY;
        Vector3 tmpPos = new Vector3(posX, posY, bonus.transform.position.z);
        bonus.transform.position = tmpPos;
    }

    // Update is called once per frame
    void Update()
    {
        bonus.GetComponent<Rigidbody2D>().velocity = -movement;
        siz.x = bonus.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = bonus.GetComponent<SpriteRenderer>().bounds.size.y;
        if (bonus.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2))
        {
            MoveBonus();
        }
    }
}
