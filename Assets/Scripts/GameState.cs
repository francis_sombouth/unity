using UnityEngine;

public class GameState : MonoBehaviour
{
    // Instance unique de la classe GameState
    public static GameState instance;

    // Score actuel
    private int score = 0;

    // Indique si l'oiseau est vivant ou non
    private bool birdAlive = true;

    // Appelé avant la création de l'objet
    private void Awake()
    {
        // Si l'instance est null, on la définit à this (l'objet courant)
        if (instance == null)
        {
            instance = this;
        }
        // Sinon, on détruit l'objet car il y a déjà une instance de la classe GameState
        else
        {
            Destroy(gameObject);
        }

        // Ne pas détruire l'objet lors du chargement d'une nouvelle scène
        DontDestroyOnLoad(gameObject);

        // Cherche tous les objets avec le tag "GameMusic"
        GameObject[] music = GameObject.FindGameObjectsWithTag("GameMusic");

        // S'il y a plus d'un objet avec le tag "GameMusic", détruire l'objet courant
        if (music.Length > 1)
        {
            Destroy(this.gameObject);
        }

        // Ne pas détruire l'objet lors du chargement d'une nouvelle scène
        DontDestroyOnLoad(this.gameObject);
    }

    // Ajoute des points au score si l'oiseau est vivant
    public void addScore(int points)
    {
        if(birdAlive)
        {        
            score += points;
            Debug.Log("Score: " + score);
        }
    }

    // Renvoie le score actuel
    public int GetScore()
    {
        return score;
    }

    // Réinitialise le score
    public void resetScore()
    {
        score = 0;
    }

    // Définit l'état de vie de l'oiseau
    public void SetBirdAlive(bool alive)
    {
        birdAlive = alive;
    }

    // Renvoie l'état de vie de l'oiseau
    public bool IsBirdAlive()
    {
        return birdAlive;
    }
}
