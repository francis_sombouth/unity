using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collideManagementBird : MonoBehaviour
{
    public string endSceneName = "End"; // Nom de la scène vers laquelle rediriger après une collision

    public static int birdCount = 0;

    // Cette fonction est appelée lorsqu'un objet entre en collision avec le collider de cet objet
    void OnTriggerEnter2D(Collider2D collision)
    {
        // Si l'objet avec lequel on entre en collision est un tuyau (objet nommé "upPipe" ou "downPipe")
        if (collision.gameObject.name == "upPipe" || collision.gameObject.name == "downPipe")
        {
            // On récupère le script "endAction" sur cet objet
            endAction endAction = GetComponent<endAction>();
            if (endAction != null)
            {
                Debug.Log("Collision");

                // On active le script "endAction" pour gérer la fin de la partie
                endAction.Activate();
            }
        }

        // Si l'objet avec lequel on entre en collision est une boîte (objet nommé "box1" ou "box2")
        if (collision.gameObject.name == "box1" || collision.gameObject.name == "box2")
        {
            Debug.Log("space");

            // On ajoute 1 point au score de la partie en cours
            GameState.instance.addScore(1);
        }

        // Si l'objet avec lequel on entre en collision est un bonus (objet nommé "bonus")
        if (collision.gameObject.name == "bonus")
        {
            Debug.Log("Bonus");

            // On ajoute 10 points au score de la partie en cours
            GameState.instance.addScore(10);

            // On détruit l'objet "bonus"
            Destroy(collision.gameObject);
        }
    }
}
