using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endAction : MonoBehaviour
{
    public touchAction touchAction; 
    public float rotationSpeed = 100f;
    public float positionExitX = -10f;
    private bool canRotate = false;

    void Start()
    {

    }

    void Update()
    {
        if (canRotate) 
        {
            transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime); 
        }

    }
    public void Activate()
    {
        touchAction = GetComponent<touchAction>();
        canRotate = true; 
        GameState.instance.SetBirdAlive(false);

        if (touchAction != null)
        {
            int score = GameState.instance.GetScore();
            Destroy(touchAction.GetComponent<touchAction>()); 
            Invoke	("goScene4",2.0f);	
            GameState.instance.addScore(score);
        }
    }
    void goScene4(){	
        SceneManager.LoadScene	("scene4-EndGame");
    
        }
    }





